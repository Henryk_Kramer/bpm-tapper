//get the buttons
let reset = document.getElementById("resetButton");
let tapper = document.getElementById("tapButton");

//variables for calculation
let lastTime;
let clickCount = 0;
let averageBPM = 0;
let allTime = 0;

//executed when reset button is clicked
reset.addEventListener("click", function(e) {
  clickCount = 0;
  averageBPM = 0;
  allTime = 0;
  tapper.innerHTML = "";
});

//executed when middle button is clicked
tapper.addEventListener("click", function(e) {
  if(clickCount != 0) {

    //get time between the clicks
    lastTime = performance.now() - lastTime;
    allTime += lastTime;
    lastTime = performance.now();

    //calculate BPM
    averageBPM = Math.round(clickCount/allTime*60*1000); //f(x) = clicks/time*timeUnit

    //display bpm
    tapper.innerHTML = averageBPM;

  } else {

    //get first time clicked
    lastTime = performance.now();

  }

  clickCount++;

});
