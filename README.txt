Author: Henryk Kramer
Created: Winter 2018
Refactored: February 2019

This program measures how fast you tap and prints the average value in BPM.

middle button: tap to get your average BPM
reset button: tap to reset the program, so you can start again